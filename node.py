import hashlib

class Node:
    def __init__(self, name):
        """
        Node initiator
        :param name: node name
        """
        name_to_use = str(name)
        self.name = name_to_use
        # self.visited = False
        self.neighbors = dict()

    def __str__(self):
        """
        string representation of node
        :return: string
        """
        str_result = ""
        str_neighbors = ""
        for name, weight in self.neighbors.items():
            str_neighbors += "Name: " + str(name) + " " + "value: " + str(weight) + " "
        str_result = "\tNode [ name: {} ][ {} neighbors {}]".format(self.name, len(self.neighbors), str_neighbors)

        return str_result

    def __len__(self):
        """
        node length = amount of neighbors
        :return: int
        """
        return len(self.neighbors)

    def __contains__(self, item):
        """
        check if a node is a neighbor
        :param item: node that might be a neighbor
        :return: boolean
        """
        return item in self.neighbors

    def __getitem__(self, key):
        """
        get node by key
        :param key: node name
        :return: node
        """
        if isinstance(key, int):
            return self.neighbors[str(key)] if str(key) in self.neighbors else None
        else:
            return self.neighbors[key] if key in self.neighbors else None

    def __eq__(self, other):
        """
        if a node equals another node
        :param other: node
        :return: boolean
        """
        return self.name == other

    def __hash__(self):
        """
        unique id for the a node (mostly used for a set collection)
        :return: hash in int
        """
        hex_result = hashlib.md5(self.name)
        result = int(hex_result.hexdigest(), 16)
        return result

    def __ne__(self, other):
        """
        if node is not equal
        :param other: node
        :return: boolean
        """
        return self.name != other

    def get_neighbors_names_list(self):
        """
        get the neighbors list of names
        :return: list of string
        """
        return self.neighbors.keys()

    def is_neighbor(self, name):
        """
        check if a node is a neighbor
        :param name: node
        :return: boolean
        """
        if isinstance(name, Node):
            return name.name in self.neighbors
        else:
            return name in self.neighbors

    def add_neighbor(self, name, weight=1):
        """
        add a neighbor to a node
        :param name: neighbor node
        :param weight: edge weight
        :return: nothing
        """
        # self.neighbors[name] = weight
        if self.name != str(name):
            self.neighbors[str(name)] = weight
        if str(name) not in self.neighbors:
            self.neighbors[str(name)] = weight

    def remove_neighbor(self, name):
        """
        remove a neighbor (edge)
        :param name: node
        :return: nothing
        """
        try:
            del self.neighbors[name]
        except KeyError:
            return "name : {} is not a neighbor of self".format(name)

    def get_weight(self, name):
        """
        return edge weight
        :param name: node
        :return: edge weight in int
        """
        key_to_seek = name
        if isinstance(name, Node):
            key_to_seek = name.name
        weight_result = self.neighbors[key_to_seek] if key_to_seek in self.neighbors else None
        return weight_result

    def is_isolated(self):
        """
        check if a node has no neighbors
        :return: boolean
        """
        return len(self.neighbors) == 0


# question_3 / question_4
def node_question_1_3_4():
    node01, node02, node03, node04, node05, node06, node07, node08, node09, node10 = load_initial_graph_nodes_q01()

    # question_3
    sum_weight = 0
    list_nodes = list()
    list_nodes.extend([node01, node02, node03, node04, node05, node06, node07, node08, node09, node10])
    for node in list_nodes:
        for neighbor in node.neighbors:
            sum_weight += node.neighbors[neighbor]

    print ("There is {} edges in the graph and the total weight is {}.".format(
                                                                                len(node01) +
                                                                                len(node02) +
                                                                                len(node03) +
                                                                                len(node04) +
                                                                                len(node05) +
                                                                                len(node06) +
                                                                                len(node07) +
                                                                                len(node08) +
                                                                                len(node09) +
                                                                                len(node10),
                                                                                sum_weight))

    # question_4

    list_nodes_sorted_by_neighbors = sorted(list_nodes, key=lambda node: node.neighbors)

    for node in list_nodes_sorted_by_neighbors:
        print (node)

# question_1
def load_initial_graph_nodes_q01():
    node01 = Node('Node01')
    node01.add_neighbor('Node02', 10)
    node01.add_neighbor('Node04', 20)
    node01.add_neighbor('Node05', 20)
    node01.add_neighbor('Node06', 5)
    node01.add_neighbor('Node07', 15)
    node02 = Node('Node02')
    node02.add_neighbor('Node03', 5)
    node02.add_neighbor('Node04', 10)
    node03 = Node('Node03')
    node03.add_neighbor('Node02', 15)
    node03.add_neighbor('Node04', 5)
    node04 = Node('Node04')
    node04.add_neighbor('Node05', 10)
    node05 = Node('Node05')
    node05.add_neighbor('Node06', 5)
    node06 = Node('Node06')
    node07 = Node('Node07')
    node07.add_neighbor('Node06', 10)
    node08 = Node('Node08')
    node08.add_neighbor('Node01', 5)
    node08.add_neighbor('Node02', 20)
    node08.add_neighbor('Node07', 5)
    node09 = Node('Node09')
    node09.add_neighbor('Node02', 15)
    node09.add_neighbor('Node08', 20)
    node09.add_neighbor('Node10', 10)
    node10 = Node('Node10')
    node10.add_neighbor('Node02', 5)
    node10.add_neighbor('Node03', 15)
    return node01, node02, node03, node04, node05, node06, node07, node08, node09, node10

# question_2
def node_test_question_2():
    # __init__(self, name)
    n = Node('Node05')
    n.neighbors['Node06'] = 20
    n.add_neighbor(7, 10)
    # n.neighbors[7] = 10

    # add_neighbor(self, name, weight=1)
    n.add_neighbor('Node09', 5)
    n.add_neighbor('Node09', 5)
    print (n)
    # remove_neighbor(self, name)
    n.remove_neighbor('Node06')

    for i in n.neighbors:
        print ("name neighbor {} ".format(i))

    # __str__(self)
    print (n)

    # __len__(self)
    print (len(n))

    # __contains__(self, item)
    print ('Node07' in n)

    # __getitem__(self, key)
    print ('getitem', n[str(7)])
    print (n['Node09'])

    # __eq__(self, other)
    print (n == 'Node05')
    print (n == 'Node06')

    # __ne__(self, other) test 01
    test_name = "Check __ne__ for equal comparison"
    expected_result = False

    actual_result = (n != 'Node05')
    test_output = "Ok" if actual_result == expected_result else "Failed"
    print("Test {} [ {} ] Expected result: [ {} ] Actual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))

    # __ne__(self, other) test 02
    test_name = "Check __ne__ for not equal comparison"
    expected_result = True

    actual_result = (n != 'Node06')
    test_output = "Ok" if actual_result == expected_result else "Failed"
    print("Test {} [ {} ] Expected result: [ {} ] Actual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))

    # is_neighbor(self, name)
    print("checking is_neighbor {} for string {} ".format(n.name, 'Node09'))
    print (n.is_neighbor('Node09'))

    print("checking is_neighbor {} for node {} ".format(n.name, 'Node09'))
    print (n.is_neighbor(Node('Node09')))

    print("checking is_neighbor {} for string {} ".format(n.name, 'Node10'))
    print (n.is_neighbor('Node10'))

    print("checking is_neighbor {} for node {} ".format(n.name, 'Node10'))
    print (n.is_neighbor(Node('Node10')))

    print("")

    # get_weight(self, name)
    print (n.get_weight('Node10'))

    # is_isolated(self)
    print (n.is_isolated())


if __name__ == '__main__':
    node_test_question_2()
    node_question_1_3_4()
