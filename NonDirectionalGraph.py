import sys
from collections import defaultdict

from graph import Graph
from node import Node
from copy import deepcopy
import ds_timer

class NonDirectionalGraph (Graph):
    def __init__(self, name, nodes=[]):
        Graph.__init__(self, name, nodes)
        self.edge_count = 0

    def add_edge(self, frm_name, to_name, weight=1):
        try:
            Graph.add_edge(self, frm_name, to_name)
            Graph.add_edge(self, to_name, frm_name)
            self.edge_count += 1
        except KeyError:
            return "frm_name node : {} or to_name node : {} not in self.".format(frm_name, to_name)

    def remove_edge(self, frm_name, to_name):
        try:
            Graph.remove_edge(self, frm_name, to_name)
            Graph.remove_edge(self, to_name, frm_name)
            self.edge_count -= 1
        except KeyError:
            return "frm_name node : {} or to_name node : {} not in self.".format(frm_name, to_name)

    def get_edge_count(self):
        return self.edge_count

def dfs_graph_scan(graph_to_scan, vertex, seen=None, path=None):
    if seen is None:
        seen = []
    if path is None:
        path = [vertex]

    seen.append(vertex)
    paths = []
    for target in graph_to_scan[vertex]:
        if target not in seen:
            target_path = path + [target]
            paths.append(tuple(target_path))
            paths.extend(dfs_graph_scan(graph_to_scan, target, seen[:], target_path))
    return paths



def question_1():
    # Question 1
    with open( r'T:\Users\yaniv\GoogleDrive\limor\DS\Part 1 - Python programming\Python programming - final project\social.txt', 'r') as f:
        group_connections = NonDirectionalGraph("Friendship_Graph")
        max_friendships = {'max_value': 0,
                           'max_line': ""
                           }

        for row in f:
            data = row.replace('.\n', '')
            data = data.split(' ')

            first_person_name = Node(data[0])   # first person
            second_person_name = Node(data[2])  # second person
            relation_operation = data[3]        # became / cancelled

            if relation_operation == "became":
                group_connections.add_node(first_person_name)
                group_connections.add_node(second_person_name)
                group_connections.add_edge(first_person_name.name, second_person_name.name)
            else:
                group_connections.remove_edge(first_person_name.name, second_person_name.name)

            if group_connections.get_edge_count() > max_friendships['max_value']:
                max_friendships['max_value'] = group_connections.get_edge_count()
                max_friendships['max_line'] = data

        print ("Max_friendships : {} , in line : {}.".format(max_friendships['max_value'], max_friendships['max_line']))

        return group_connections

def question_2():
    # Question 2
    with open(r'T:\Users\yaniv\GoogleDrive\limor\DS\Part 1 - Python programming\Python programming - final project\social.txt', 'r') as f:
        group_connections_reuben = NonDirectionalGraph("Friendship_Graph")

        max_friendships_reuben = {'max_value': 0,
                                  'max_line': ""
                                  }
        for row in f:
            data = row.replace('.\n', '')
            data = data.split(' ')

            first_person_name = Node(data[0])   # first person
            second_person_name = Node(data[2])  # second person
            relation_operation = data[3]        # became / cancelled

            if (first_person_name == 'Reuben' or second_person_name == 'Reuben') and relation_operation == 'became':
                group_connections_reuben.add_node(first_person_name)
                group_connections_reuben.add_node(second_person_name)
                group_connections_reuben.add_edge(first_person_name.name, second_person_name.name)

            elif (first_person_name == 'Reuben' or second_person_name == 'Reuben') and relation_operation == 'cancelled':
                group_connections_reuben.remove_edge(first_person_name.name, second_person_name.name)

            if group_connections_reuben.get_edge_count() > max_friendships_reuben['max_value']:
                max_friendships_reuben['max_value'] = group_connections_reuben.get_edge_count()
                max_friendships_reuben['max_line'] = data
        print ("Max_friendships with Reuben : {} , in line : {}.".format(max_friendships_reuben['max_value'],
                                                                         max_friendships_reuben['max_line']))

# Question 3
def find_maximal_path(graph):
    list_result = list()
    for frm_name in graph.nodes:
        for to_name in graph.nodes:
            if frm_name == to_name:
                continue
            else:
                print ("Find_maximal_path - start:", frm_name, to_name)
                list_result.append((maximal_path(graph, graph[frm_name], graph[to_name], fmp_visited=[]), frm_name, to_name))
                print (list_result)
    list_result.sort(reverse=True)
    max_path = list_result[0]
    return max_path

def maximal_path(graph, frm_name, to_name, fmp_visited=[]):
    # print("** Find_maximal_path - start:", frm_name.name, to_name.name, len(fmp_visited))
    current_fmp_visited = deepcopy(fmp_visited)
    current_fmp_visited.append(frm_name)
    weight_list = list()
    weight_result = None

    if frm_name == to_name:
        return 0

    # list comprehension of possible paths (reachable_neighbors)
    reachable_neighbors = [graph[neighbor_name] for neighbor_name in frm_name.neighbors
          if graph[neighbor_name] not in current_fmp_visited and graph.is_reachable(graph[neighbor_name], to_name)]

    # print("reachable_neighbors", reachable_neighbors.__repr__())
    # direct neighbor
    if frm_name.is_neighbor(to_name.name):
        weight_list.append(frm_name.get_weight(to_name))

    # loop over all nodes in reachable neighbors
    for node_neighbor in reachable_neighbors:
        path_weight = maximal_path(graph,node_neighbor, to_name, current_fmp_visited)
        if path_weight is not None:
            weight_list.append(frm_name.get_weight(node_neighbor) + path_weight)

    if len(weight_list) > 0:
        weight_list.sort(reverse=True)
        weight_result = weight_list[0]

    return weight_result


# Question 4
def suggest_friend(graph, node_name):
    count_common_friends = 0
    max_common_friends = {'max_friends': 0,
                          'node_name': ""}
    for node in graph.nodes.values():
        if node.name not in graph.nodes[node_name].neighbors and node.name != node_name:
            for neighbor in node.neighbors:
                if neighbor in graph.nodes[node_name].neighbors:
                    count_common_friends += 1

        if count_common_friends > max_common_friends['max_friends']:
            max_common_friends['max_friends'] = count_common_friends
            max_common_friends['node_name'] = node.name

    return max_common_friends['node_name']

def test(graph):
    counter = 0
    for frm_name in graph.nodes:
        for to_name in graph.nodes:
            if frm_name == to_name:
                continue
            print (frm_name, to_name)
            counter += 1
    print counter

# def create_nondirectionalgraph():
#     # Create Nodes
#     node01 = Node('Node01')
#     node01.neighbors['Node02'] = 10
#     node01.neighbors['Node04'] = 20
#     node01.neighbors['Node05'] = 20
#     node01.neighbors['Node06'] = 5
#     node01.neighbors['Node07'] = 15
#
#     node02 = Node('Node02')
#     node02.add_neighbor('Node03', 5)
#     node02.add_neighbor('Node04', 10)
#
#     node03 = Node('Node03')
#     node03.add_neighbor('Node02', 15)
#     node03.add_neighbor('Node04', 5)
#
#     node04 = Node('Node04')
#     node04.add_neighbor('Node05', 10)
#
#     node05 = Node('Node05')
#     node05.add_neighbor('Node06', 5)
#
#     node06 = Node('Node06')
#
#     node07 = Node('Node07')
#     node07.add_neighbor('Node06', 10)
#
#     node08 = Node('Node08')
#     node08.add_neighbor('Node01', 5)
#     node08.add_neighbor('Node02', 20)
#     node08.add_neighbor('Node07', 5)
#
#     node09 = Node('Node09')
#     node09.add_neighbor('Node02', 15)
#     node09.add_neighbor('Node08', 20)
#     node09.add_neighbor('Node10', 10)
#
#     node10 = Node('Node10')
#     node10.add_neighbor('Node02', 5)
#     node10.add_neighbor('Node03', 15)
#
#     new_graph = NonDirectionalGraph('test', [node01, node02, node03, node04, node05, node06, node07, node08, node09, node10])
#
#     # node04 = Node('Node04')
#     # node04.add_neighbor('Node05', 10)
#     # new_graph.add_node(node04)
#     #
#     # new_graph.add_edge('Node04', 'Node05')
#     # new_graph.remove_edge('Node04', 'Node05')
#
#     # print(new_graph)
#     return new_graph

if __name__ == '__main__':
    my_timer = ds_timer.RunningTimer()
    # question_1()
    # question_2()

    # Question 4
    # total_graph = question_1()
    # friend_name = suggest_friend(total_graph, 'Reuben')
    # print("Suggest friend is: {} \n".format(str(friend_name)))

    # Question 3
    total_graph = question_1()
    # print(type(total_graph), len(total_graph.nodes))
    # print(total_graph)

    dd_graph = total_graph.graph_as_defaultdict()

    # get all the possible paths in the graph
    all_paths = dfs_graph_scan(dd_graph, 'Manasseh')

    # all_paths = total_graph.dfs_all_paths_non_dir(vertex='Manasseh')

    # print the amount of all possible paths in the graph
    print(len(all_paths))

    # max of a list comprehension - of all the paths length
    max_len = max(len(path) for path in all_paths)

    # list comprehension get a list of the maximum path length paths
    max_paths = [path for path in all_paths if len(path) == max_len]

    print(max_len, len(max_paths))
    # print example of the first path found (with maximum length)
    print(max_paths[0])
    my_timer.stop()
    print(my_timer.display())


    # sys.exit()
    # weight = find_maximal_path(total_graph)
    # print("Final Result : {} \n".format(str(weight)))

    # print test(question_1())
    # max_distance = {'max_distance_value': 0,
    #                 'max_distance_members_name': [],
    #                 }
    # for source_node in total_graph.nodes.values():
    #     for target_node in total_graph.nodes.values():
    #         # print '.',
    #         # print (source_node.name, target_node.name)
    #         if source_node is target_node or not total_graph.is_reachable(source_node, target_node):
    #             print('Skipping pair ', source_node.name, target_node.name)
    #             continue
    #         print "Finding longest path between {} and {} ...".format(source_node.name, target_node.name),
    #         # distance = total_graph.find_longest_path_bfs(source_node, target_node)
    #         distance = total_graph.get_max_distance(source_node, target_node)
    #         print " the distance is ", distance
    #         if distance is not None and distance > max_distance['max_distance_value']:
    #             max_distance['max_distance_value'] = distance
    #             max_distance['max_distance_members_name'] = [source_node.name, target_node.name]
    #
    # print('')
    #
    # print(max_distance)

    # print suggest_friend(create_nondirectionalgraph(), 'Node03')
    # create_nondirectionalgraph()
    # total_graph = create_nondirectionalgraph()
    # weight = find_maximal_path(total_graph)
    # print("Final Result: {} \n".format(str(weight)))


    # # print("XXX Testing 3 - 4 ")
    # weight = find_maximal_path(total_graph,total_graph['Node03'], total_graph['Node04'])
    # print("Final Result 3 - 4: {} \n".format(str(weight)))
    #
    # # print("XXX Testing 3 - 4 ")
    # weight = find_maximal_path(total_graph,total_graph['Node03'], total_graph['Node04'])
    # print("Final Result 3 - 4: {} \n".format(str(weight)))



