

files_locations = {
    'home': r"t:\Users\yaniv\Dropbox\Dropbox\Naya -DS - June 2017",
    'work': r"C:\Users\limorlev\Documents",
    'class': r"C:\Users\student5\Documents\DS_06_2017"
}

location = "home"
# location = "work"
# location = "class"

FilePath_queue_ds = files_locations[location] + r"\Part 1 - Python programming\08 - Object-Oriented Programming\01 - Basics\queue.txt"
FilePath_queue = files_locations[location] + r"\Part 1 - Python programming\08 - Object-Oriented Programming\01 - Basics\queue.txt"


FilePath_queue_ds = r"C:\Users\student5\Documents\DS_06_2017\Part 1 - Python programming\08 - Object-Oriented Programming\01 - Basics\queue.txt"
FilePath_queue = r"C:\Users\limorlev\Documents\Part 1 - Python programming\08 - Object-Oriented Programming\01 - Basics\queue.txt"

FilePath_queue2 = r"C:\Users\limorlev\Documents\Part 1 - Python programming\08 - Object-Oriented Programming\02 - Magic functions\queue2.txt"
FilePath_queue2_ds = r"C:\Users\student5\Documents\DS_06_2017\Part 1 - Python programming\08 - Object-Oriented Programming\02 - Magic functions\queue2.txt"

FilePath_queue3 = r"C:\Users\limorlev\Documents\Part 1 - Python programming\08 - Object-Oriented Programming\03 - Inheritance\queue3.txt"
FilePath_queue3_ds = r"C:\Users\student5\Documents\DS_06_2017\Part 1 - Python programming\08 - Object-Oriented Programming\03 - Inheritance\queue3.txt"

path_error_handling = r"C:\Users\limorlev\Documents\Part 1 - Python programming\09 - Error handling\exercise1"

# 08 - Object-Oriented Programming - Basics , Magic functions


class Person:
    def __init__(self,name, id_number, gender, year_of_birth, current_year=2016):

        self.name = name
        self.id_number = id_number
        self.gender = gender
        self.year_of_birth = year_of_birth

        self.age = current_year - self.year_of_birth
        self.is_alive = True
        self.year_of_death = None
        self.is_married = False
        self.spouse_name = None
        self.children = []

    def __str__(self):
        result = "name: {}, id_number: {}, gender: {}, year_of_birth: {}, age: {}, is_alive: {}, year_of_death: {} ," \
                " is_married: {}, spouse_name: {}, children: {} ".format(
                                                                       self.name,
                                                                       self.id_number,
                                                                       self.gender,
                                                                       self.year_of_birth,
                                                                       self.age,
                                                                       self.is_alive,
                                                                       self.year_of_death,
                                                                       self.is_married,
                                                                       self.spouse_name,
                                                                       self.children)
        return result

    def __eq__(self, other):
        return self.id_number == other

    def __ne__(self, other):
        return not (self.id_number == other)

    def __lt__(self, other):
        return self.age < other

    def __gt__(self, other):
        return self.age > other

    def __le__(self, other):
        return not (self.age > other)

    def __ge__(self, other):
        return not (self.age < other)

    def grow(self, n):
        self.age += n
        return self.age

    def got_married(self, spouse_name):
        self.spouse_name = spouse_name
        self.is_married = True
        return self.spouse_name

    def begat(self, *children):
        for child in children:
            self.children.append(child)
        return self.children

    def die(self, year_of_death=None):
        self.year_of_death = year_of_death
        if self.year_of_death!= None:
            self.is_alive = False
            self.age = self.year_of_death - self.year_of_birth
        return self.year_of_death


def person_tests():
    p = Person('limor', 1234, 'f', 1985)
    print p.grow(2)
    print p.got_married('yaniv')
    print p.begat('adi', 'omer')
    print p.die()
    print p
    print p == 1234
    print p > 31
    print p < 40


class Queue:
    def __init__(self):
        self._queue = []

    def __getitem__(self, item):
        if 0 <= item <= len(self._queue):
            return self._queue[item]

    def __str__(self):
        part_list = self._queue[:3]
        return ' | '.join(part_list)

    def __eq__(self, other):
        return len(self._queue) == len(other)

    def __ne__(self, other):
        return not (len(self._queue) == len(other))

    def __lt__(self, other):
        return len(self._queue) < len(other)

    def __gt__(self, other):
        return len(self._queue) > len(other)

    def __le__(self, other):
        return not (len(self._queue) > len(other))

    def __ge__(self, other):
        return not (len(self._queue) < len(other))

    def __radd__(self, other):
        new_queue = Queue()
        new_queue._queue = other._queue + self._queue
        return new_queue

    def is_empty(self):
        return len(self._queue) == 0

    def enqueue(self, element):
        self._queue.insert(0, element)

    def dequeue(self):
        # if len(self.queue)!= 0:
        if not self.is_empty():
            return self._queue.pop()

    def __len__(self):
        return len(self._queue)


def queue_tests():
    q = Queue()
    q.enqueue('limor')
    q.enqueue('yaniv')
    print q[0]
    print q
    print q > [1]
    print q == [1, 2]
    tmp_queue = Queue()
    tmp_queue.enqueue('mama')
    q2 = q + tmp_queue
    print q2


    print q.is_empty()
    print q.dequeue()
    print q.get_len()



# 08 - Object-Oriented Programming -  Inheritance

queue_patient = Queue()
waiting = []
with open(FilePath_queue) as f:
    for line in f:
        if "Call" not in line:
            patient = line.strip()
            queue_patient.enqueue(patient)
            waiting.append((patient, len(queue_patient)))
            # print waiting
            # print queue_patient.queue
        else:
            queue_patient.dequeue()
    print sorted(waiting, key=lambda x: x[1])[-1][0]

queue_patient_doctor1 = Queue()
queue_patient_doctor2 = Queue()
waiting1 = []
waiting2 = []

with open(FilePath_queue2) as f:
    for line in f:
        if "Call" not in line:
            patient = line.strip()
            if queue_patient_doctor1 > queue_patient_doctor2:
                queue_patient_doctor2.enqueue(patient)
                waiting2.append((patient, len(queue_patient_doctor2)))
            else:
                queue_patient_doctor1.enqueue(patient)
                waiting1.append((patient, len(queue_patient_doctor1)))

        else:
            if "Call1" in line:
                queue_patient_doctor1.dequeue()
            elif "Call2" in line:
                queue_patient_doctor2.dequeue()

    worst1 = sorted(waiting1, key=lambda x: x[1])[-1]
    worst2 = sorted(waiting2, key=lambda x: x[1])[-1]

    print worst1
    print worst2

    worst = worst1[0] if worst1[1] > worst2[1] else worst2[0]

    print worst


class Israeli(Person):
    def __init__(self, served, name, id_number, gender, year_of_birth, current_year=2016):
        Person.__init__(self, name, id_number, gender, year_of_birth, current_year=2016)
        self.served = False

    def serve(self, keva=0):
        years = (3 + keva) if (self.gender == 'm') else (2 + keva)
        self.grow(years)
        self.served = True

    def __str__(self):
        return Person.__str__(self) + "served: {}".format(self.served)

def israeli_tests():
    pi = Israeli(True,'limor', 1234, 'f', 1985)
    pi.serve(1)
    print pi


class GroupsQueue(Queue):
    def __init__(self):
        Queue.__init__(self)

    def enqueue(self, *elements):
        for element in elements:
            Queue.enqueue(self, element)


def groups_queue_test():
    waiting_patient = []
    patients = GroupsQueue()
    with open(FilePath_queue3) as f:
        for line in f:
            if "Call" not in line:
                list_patients = [name.strip() for name in line.split(',')]
                patients.enqueue(list_patients)
                waiting_patient.append((len(patients), list_patients[-1]))
            else:
                patients.dequeue()
    print (sorted(waiting_patient))[-1][1]


# 09 - Error handling

def error_handing_exercise():
    with open(path_error_handling + "\\file_names.txt") as main_f:
        files_name = main_f.read().split(';')
        for file in files_name:
            try:
                with open(path_error_handling + "\\" + file) as f:
                    for i, line in enumerate(f, 1):
                        try:
                            num1, num2, num3 = [float(x) for x in line.split(',')]
                            if num1/num2 == num3:
                                print ("{}/{}={}".format(num1, num2, num3))
                            else:
                                print ("{}/{}!={}".format(num1, num2, num3))
                        except ValueError as err:
                            print ("{} , line {}: {}".format(file, i, err))
                        except ZeroDivisionError as err:
                            print ("{} , line {}: {}".format(file, i, err))
            except IOError as err:
                print "{}: {}".format(file, err)


if __name__ == "__main__":
    groups_queue_test()
    # main()





