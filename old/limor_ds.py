class Person:
    def __init__(self,name, id_number, gender, year_of_birth, current_year=2016):

        self.name = name
        self.id_number = id_number
        self.gender = gender
        self.year_of_birth = year_of_birth

        self.age = current_year - self.year_of_birth
        self.is_alive = True
        self.year_of_death = None
        self.is_married = False
        self.spouse_name = None
        self.children = []

    def __str__(self):
        return "name: {} , id_number: {} , gender: {} , year_of_birth: {} , age: {} , is_alive: {} , year_of_death: {} , is_married: {} , spouse_name: {} , children: {} ".format(
                self.name, self.id_number, self.gender, self.year_of_birth, self.age, self.is_alive, self.year_of_death, self.is_married, self.spouse_name, self.children)

    def __eq__(self, other):
        return self.id_number == other

    def __lt__(self, other):
        return self.age < other

    def __gt__(self, other):
        return self.age > other

    def grow(self, n):
        self.age += n
        return self.age

    def got_married(self, spouse_name):
        self.spouse_name = spouse_name
        self.is_married = True
        return self.spouse_name

    def begat(self, *children):
        for child in children:
            self.children.append(child)
        return self.children

    def die(self, year_of_death=None):
        self.year_of_death = year_of_death
        if self.year_of_death != None:
            self.is_alive = False
            self.age = self.year_of_death - self.year_of_birth
        return self.year_of_death

# p = Person('limor', 1234, 'f', 1985)
# print p.grow(2)
# print p.got_married('yaniv')
# print p.begat('adi', 'omer')
# print p.die()
# print p
# print p == 1234
# print p > 31
# print p < 40


class Queue:
    def __init__(self):
        self.queue = []

    def __getitem__(self, item):
        if 0 <= item <= len(self.queue):
            return self.queue[item]

    def __str__(self):
        if len(self.queue):
            return self.queue[0]+ self.queue[1]+ self.queue[2]

    def is_empty(self):
        return len(self.queue) == 0

    def enqueue(self, element):
        self.queue.insert(0, element)

    def dequeue(self):
        # if len(self.queue)!= 0:
        if not self.is_empty():
            return self.queue.pop()

    def __len__(self):
        return len(self.queue)

q = Queue()
q.enqueue('limor')
q.enqueue('adi')
q.enqueue('ram')
print q[0]
print q
# print q.is_empty()
# print q.dequeue()
# print q.get_len()

queue_patient = Queue()
waiting = []
with open(r"T:\Users\yaniv\GoogleDrive\limor\DS\Part 1 - Basic Python\08 - Object-Oriented Programming\01 - Basics\queue.txt") as f:
    for line in f:
        if "Call" not in line:
            patient = line.strip()
            queue_patient.enqueue(patient)
            waiting.append((patient, len(queue_patient)))
            # print waiting
            # print queue_patient.queue
        else:
            queue_patient.dequeue()
    print sorted(waiting, key=lambda x: x[1])[-1][0]



