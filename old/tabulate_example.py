from tabulate import tabulate as tb

my_list = [['First line'], ['Second line'], ['Third line']]
my_string_result = tb(my_list, tablefmt="grid")

print(my_string_result)
