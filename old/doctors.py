waiting_room = list()
doc01 = list()
doc02 = list()

with open('queue2.txt') as input_file:
    for line in input_file:
        print("Processing {} ".format(line.strip()))
        if "Call" not in line:
            waiting_room.append(line.strip())
            print("Adding {} to waiting room".format(line.strip()), waiting_room)
        else:
            if line.strip() == "Call1":
                patient = waiting_room.pop(0)
                doc01.append(patient)
                print("Sending {} to doc01".format(patient), doc01)
            else:
                patient = waiting_room.pop(0)
                doc02.append(patient)
                print("Sending {} to doc02".format(patient), doc02)


print("Waiting room:", waiting_room)
print("Doc01:", doc01)
print("Doc02:", doc02)
