from node import Node
from copy import deepcopy
import traceback
import json

from collections import defaultdict

class Graph:
    def __init__(self, name, nodes=[]):
        """
        Graph initiator
        :param name: the graph name
        :param nodes: a list of nodes building the graph
        """
        self.name = name
        self.nodes = dict()
        # self.debug = True
        self.debug = False
        for node in nodes:
            self.nodes[node.name] = node

    def __str__(self):
        """
        String representation of graph
        :return: string
        """
        str_nodes = ''
        for node in self.nodes.values():
            str_nodes = str(node) + '\n' + str(str_nodes)
        str_output = "Graph name: {} \nNodes in graph: \n{}".format(self.name, str_nodes)
        return str_output

    def __len__(self):
        """
        The amount of nodes in the graph
        :return:
        """
        return len(self.nodes)

    def __contains__(self, key):
        """
        check if a node is contained in the graph
        :param key: node
        :return: boolean
        """
        result = False
        if isinstance(key, str) and key in self.nodes:
            result = True
        elif isinstance(key, Node) and key in self.nodes.values():
            result = True
        return result

    def __getitem__(self, name):
        """
        get a node item from a graph
        :param name: node or node name
        :return: node
        """
        # print('graph - getitem type', type(name), self.nodes)
        try:
            if name not in self.nodes.values():
                raise KeyError("The node name: {} not in the graph.".format(name))
            else:
                return self.nodes[name]
        except TypeError as e:
                print("TypeError with ", name, e)
        except KeyError as e:
            print("KeyError with ", name, e)
            traceback.print_exc()

    def __add__(self, other):
        """
        add a graph or a node to a graph
        :param other: graph or node
        :return: graph
        """
        if isinstance(other, Graph):
            for node in other.nodes.values():
                self.add_node(node)
        elif isinstance(other, Node):
            self.add_node(other)
        return self

    def add_node(self, node):
        """
        add a node to a graph
        :param node: node
        :return: nothing
        """
        if isinstance(node, Node):
            self.nodes[node.name] = node if node not in self else self[node.name]
            for neighbor_node_name, neighbor_weight in node.neighbors.items():
                if neighbor_node_name in self:
                    self[node.name].add_neighbor(neighbor_node_name, neighbor_weight)
                else:
                    self.nodes[neighbor_node_name] = Node(neighbor_node_name)

    def remove_node(self, name):
        """
        remove a node from a graph
        :param name: node
        :return: nothing
        """
        try:
            for node in self.nodes.values():
                if node.is_neighbor(name):
                    node.remove_neighbor(name)

            del self.nodes[name]
        except KeyError:
            return "name : {} not in self".format(name)

    def is_edge(self, frm_name, to_name):
        """
        check if there is an edge between two nodes
        :param frm_name: source node
        :param to_name: target node
        :return: boolean
        """
        try:
            node = self[frm_name]
            return node.is_neighbor(to_name)
        except KeyError:
            return "frm_name : {} not in self".format(frm_name)

    def add_edge(self, frm_name, to_name, weight=1):
        """
        add an edge between two nodes
        :param frm_name: source node
        :param to_name: target node
        :param weight: the edge weight
        :return: nothing
        """
        try:
            node = self[frm_name]
            node.add_neighbor(to_name, weight)
        except KeyError:
            return "frm_name : {} not in self".format(frm_name)

    def remove_edge(self, frm_name, to_name):
        """
        remove an edge from two nodes
        :param frm_name: source node
        :param to_name: target node
        :return: nothing
        """
        try:
            node = self[frm_name]
            node.remove_neighbor(to_name)
        except KeyError:
            return "frm_name : {} not in self".format(frm_name)

    def get_edge_weight(self, frm_name, to_name):
        """
        return the edge weight between two nodes
        :param frm_name: source node
        :param to_name: target node
        :return: edge weight between the nodes
        """
        try:
            frm_node = self[frm_name]
            return frm_node.get_weight(to_name)
        except KeyError:
            return "frm_name : {} not in self".format(frm_name)

    def get_path_weight(self, path):
        """
        sums all the paths weight (from all the edges)
        :param path: list of nodes
        :return: sum of weights (int)
        """
        if len(path) == 0:
            return None
        try:
            list_nodes = [self[node_name] for node_name in path]
        except KeyError:
            return None
        previous = list_nodes[0]
        node_counter = 1
        weight_total = 0
        while node_counter < len(list_nodes):
            if previous.is_neighbor(path[node_counter]):
                weight_total += previous.get_weight(path[node_counter])
            else:
                return None
            previous = list_nodes[node_counter]
            node_counter += 1

        return weight_total

    def is_reachable(self, frm_name, to_name, visited=[]):
        """
        check if a node is reachable from another node
        :param frm_name: source node
        :param to_name: target node
        :param visited: internal parameter for the nodes already visited
        :return: boolean
        """
        # node is reachable to himself
        if frm_name == to_name:
            return True
        current_visited = deepcopy(visited)
        current_visited.append(frm_name)
        current_neighbor_reachable = False
        route_reachable = False
        if frm_name.is_neighbor(to_name.name):
            route_reachable = True
        else:
            for neighbor in frm_name.neighbors:
                if neighbor not in current_visited:
                    if isinstance(neighbor, str):
                        current_neighbor_reachable = self.is_reachable(self[neighbor], to_name, current_visited)
                    else:
                        current_neighbor_reachable = self.is_reachable(neighbor, to_name, current_visited)
                    route_reachable = current_neighbor_reachable if current_neighbor_reachable else route_reachable
        return route_reachable

    def find_shortest_path(self, frm_name, to_name, fsp_visited=[]):
        """
        a recursive solution to finding the shortest path beetween two nodes in a graph
        :param frm_name: source node
        :param to_name: target node
        :param fsp_visited: internal visited nodes
        :return: path weight (total)
        """
        if self.debug:
            print("** Find_shortest_path - start:", frm_name.name, to_name.name, len(fsp_visited))
        current_fsp_visited = deepcopy(fsp_visited)
        current_fsp_visited.append(frm_name)
        weight_list = list()
        weight_result = None

        if frm_name == to_name:
            return 0

        # list comprehension of possible paths (reachable_neighbors)
        reachable_neighbors = [self[neighbor_name] for neighbor_name in frm_name.neighbors
                if self[neighbor_name] not in current_fsp_visited and self.is_reachable(self[neighbor_name], to_name)]

        if self.debug:
            print("reachable_neighbors", reachable_neighbors.__repr__())
        # direct neighbor
        if frm_name.is_neighbor(to_name.name):
            weight_list.append(frm_name.get_weight(to_name))

        # loop over all nodes in reachable neighbors
        for node_neighbor in reachable_neighbors:
            path_weight = self.find_shortest_path(node_neighbor, to_name, current_fsp_visited)
            if path_weight is not None:
                weight_list.append(frm_name.get_weight(node_neighbor) + path_weight)

        if len(weight_list) > 0:
            weight_list.sort()
            weight_result = weight_list[0]

        if self.debug and len(weight_list) > 1:
            print(weight_list)

        return weight_result

    def find_longest_path(self, frm_name, to_name, fsp_visited=[]):
        """
        find the longest path - recursive version
        :param frm_name: source node
        :param to_name: target node
        :param fsp_visited: internal - visited nodes
        :return: int - longest path distance
        """
        if self.debug:
            print("** Find_longest_path - start:", frm_name.name, to_name.name, len(fsp_visited))

        current_fsp_visited = deepcopy(fsp_visited)
        current_fsp_visited.append(frm_name)
        weight_list = list()
        weight_result = None

        if frm_name == to_name:
            return 0

        # list comprehension of possible paths (reachable_neighbors)
        reachable_neighbors = [self[neighbor_name] for neighbor_name in frm_name.neighbors
                               if
                               self[neighbor_name] not in current_fsp_visited and self.is_reachable(self[neighbor_name],
                                                                                                    to_name)]

        if self.debug:
            print("reachable_neighbors", reachable_neighbors.__repr__())
        # direct neighbor
        if frm_name.is_neighbor(to_name.name):
            weight_list.append(frm_name.get_weight(to_name))

        # loop over all nodes in reachable neighbors
        for node_neighbor in reachable_neighbors:
            path_weight = self.find_longest_path(node_neighbor, to_name, current_fsp_visited)
            if path_weight is not None:
                weight_list.append(frm_name.get_weight(node_neighbor) + path_weight)

        if len(weight_list) > 0:
            weight_list.sort()
            weight_result = weight_list[-1]

        if self.debug and len(weight_list) > 1:
            print(weight_list)

        return weight_result

    def find_shortest_path_bfs(self, frm_name, to_name, fsp_visited=[]):
        """
        find the shortest path - breadth first by finding all paths and then sorting their weight
        :param frm_name: source node
        :param to_name: target node
        :param fsp_visited: internal - visited nodes
        :return: int - path distance
        """
        all_paths_distance = list()
        for path in self.get_all_paths(frm_name, to_name):
            all_paths_distance.append(self.get_path_weight(path))

        if len(all_paths_distance) == 0:
            return None
        else:
            all_paths_distance.sort()
            return all_paths_distance[0]

    def find_longest_path_bfs(self, frm_name, to_name, fsp_visited=[]):
        """
        find longest path with breadth first - find all paths and sort the maximum
        :param frm_name: source node
        :param to_name: target node
        :param fsp_visited: internal visited nodes
        :return: longest path weight
        """
        all_paths_distance = list()
        for path in self.get_all_paths(frm_name, to_name):  # get the path from all_paths generator
            all_paths_distance.append(self.get_path_weight(path))

        if len(all_paths_distance) == 0:
            return None
        else:
            all_paths_distance.sort()
            return all_paths_distance[-1]

    def get_max_distance(self, source, target):
        """
        find longest path with depth first search and recursion
        :param source: source node
        :param target: target node
        :return: the path with the maximum nodes on the way
        """
        stack = [(source, [source], 0)]
        max_distance = None
        unvisited_nodes_set = {self[node] for node in self.nodes}

        while stack:
            (node, path, distance) = stack.pop()

            if node in unvisited_nodes_set:
                unvisited_nodes_set.remove(node)

            neighbors_set = {self[node] for node in self[str(node.name)].get_neighbors_names_list()}
            for next in neighbors_set:
                if next == target:
                    current_distance = distance + node.get_weight(next.name)
                    max_distance = current_distance if max_distance is None or current_distance > max_distance else max_distance
                else:
                    if next in unvisited_nodes_set:
                        stack.append((next, path + [next], distance + node.get_weight(next.name)))

        return max_distance

    def get_min_distance(self, source, target):
        """
        find shortest path with depth first search and recursion
        :param source: source node
        :param target: target node
        :return: the path with the minimum nodes on the way
        """
        stack = [(source, [source], 0)]
        min_distance = None
        unvisited_nodes_set = {self[node] for node in self.nodes}

        while stack:
            (node, path, distance) = stack.pop()

            if node in unvisited_nodes_set:
                unvisited_nodes_set.remove(node)

            neighbors_set = {self[node] for node in self[str(node.name)].get_neighbors_names_list()}

            for next in neighbors_set:
                if next == target:
                    current_distance = distance + node.get_weight(next.name)
                    min_distance = current_distance if min_distance is None or current_distance < min_distance else min_distance
                else:
                    if next in unvisited_nodes_set:
                        stack.append((next, path + [next], distance + node.get_weight(next.name)))

        return min_distance

    def get_all_paths(self, source, target):
        """
        find all the possible paths with a generator and recursion
        :param source: source node
        :param target: target node
        :return: list of paths
        """
        stack = [(source, [source])]
        # print("stack :", len(stack), stack, stack[0])
        while stack:
            # print '#',
            (node, path) = stack.pop()
            unvisited_nodes_set = {self[node] for node in self[str(node.name)].get_neighbors_names_list()} - set(path)

            for next in unvisited_nodes_set:
                if next == target:
                    yield path + [next]     # generator yield
                else:
                    stack.append((next, path + [next]))

    def get_path_weight(self, path):
        """
        sum the weight of the total edges on a path
        :param path: path
        :return: int - total weights
        """
        weight_sum = 0
        counter = 0
        for node in path:
            counter += 1
            if node != path[-1]:
                weight_sum += node.get_weight(path[counter].name)
        return weight_sum

    def graph_as_defaultdict(self):
        """
        produce a default dictionary from the graph
        :return: defaultdict
        """
        result_graph = defaultdict(list)
        for node in self.nodes:
            node_name = self[node].name
            neighbors_list = self[node].get_neighbors_names_list()
            for neighbor in neighbors_list:
                result_graph[node_name].append(neighbor)
        return result_graph

    # question_3
    def sort_number_reachable_node(self):
        nodes_dict = dict()
        for node in self.nodes:
            nodes_dict[node] = 0
        for node_frm in nodes_dict:
            for node_to in nodes_dict:
                if node_frm == node_to:
                    continue
                if self.is_reachable(self[node_frm], self[node_to], visited=[]):
                    nodes_dict[node_to] += 1
        return sorted(nodes_dict.items(), key=lambda x: x[1])

    # question_4
    def shortest_path_highest_weight(self):
        pair_nodes_dict = dict()
        for node_frm in self.nodes:
            for node_to in self.nodes:
                pair_nodes_dict[(node_frm, node_to)] = self.find_shortest_path(self.nodes[node_frm], self.nodes[node_to])
        return sorted(pair_nodes_dict.items(), key=lambda x: x[1], reverse=True)[0]

    def dfs_all_paths_non_dir(self, vertex, seen=None, path=None):
        """
        function to find a depth first scan of a graph
        :param vertex: node
        :param seen: internal - nodes visited already
        :param path: path of nodes so far from the source
        :return: all paths
        """
        graph_as_dd = self.graph_as_defaultdict()

        if seen is None:
            seen = []
        if path is None:
            path = [vertex]

        seen.append(vertex)

        paths = []
        for target in graph_as_dd[vertex]:
            if target not in seen:
                target_path = path + [target]
                paths.append(tuple(target_path))
                paths.extend(dfs_graph_scan(graph_as_dd, target, seen[:], target_path))
                # paths.extend(self.dfs_all_paths_non_dir(target, seen[:], target_path))
        return paths


def dfs_graph_scan(graph_to_scan, vertex, seen=None, path=None):
    """
    function to find a depth first scan of a graph
    :param graph_to_scan: graph
    :param vertex: node
    :param seen: internal - nodes visited already
    :param path: path of nodes so far from the source
    :return: all paths
    """
    if seen is None:
        seen = []
    if path is None:
        path = [vertex]

    seen.append(vertex)

    paths = []
    for target in graph_to_scan[vertex]:
        if target not in seen:
            target_path = path + [target]
            paths.append(tuple(target_path))
            paths.extend(dfs_graph_scan(graph_to_scan, target, seen[:], target_path))
    return paths

# COUNTER_FEEDBACK = 10000

# question_1
def load_graph(graph_name):
    # Create Nodes
    node01 = Node('Node01')
    node01.add_neighbor('Node02', 10)
    node01.add_neighbor('Node04', 20)
    node01.add_neighbor('Node05', 20)
    node01.add_neighbor('Node06', 5)
    node01.add_neighbor('Node07', 15)

    node02 = Node('Node02')
    node02.add_neighbor('Node03', 5)
    node02.add_neighbor('Node04', 10)

    node03 = Node('Node03')
    # circle in graph
    node03.add_neighbor('Node02', 15)
    node03.add_neighbor('Node04', 5)

    node04 = Node('Node04')
    node04.add_neighbor('Node05', 10)

    node05 = Node('Node05')
    node05.add_neighbor('Node06', 5)

    node06 = Node('Node06')
    # test - node06.add_neighbor('Node11', 11)

    node07 = Node('Node07')
    node07.add_neighbor('Node06', 10)

    node08 = Node('Node08')
    node08.add_neighbor('Node01', 5)
    node08.add_neighbor('Node02', 20)
    node08.add_neighbor('Node07', 5)

    node09 = Node('Node09')
    node09.add_neighbor('Node02', 15)
    node09.add_neighbor('Node08', 20)
    node09.add_neighbor('Node10', 10)

    node10 = Node('Node10')
    node10.add_neighbor('Node02', 5)
    node10.add_neighbor('Node03', 15)

    # create instance graph
    new_graph1 = Graph(graph_name, [node01, node02, node03])
    new_graph2 = Graph(graph_name, [node04, node05, node06, node07])
    new_graph3 = Graph(graph_name, [node08, node09, node10])
    total_graph = new_graph1 + new_graph2 + new_graph3
    print(total_graph)
    # json.dumps(total_graph.nodes)

    # all_nodes_graph = Graph('python_exercise', [node01, node02, node03, node04, node05,
    #                                             node06, node07, node08, node09, node10])
    # return all_nodes_graph
    return total_graph
# question_3
def question_3():
    print load_graph().sort_number_reachable_node()
# question_4
def question_4():
    print load_graph().shortest_path_highest_weight()

def check_longest_graph():
    long_graph = load_graph("Go long")

    test_graph = long_graph.graph_as_defaultdict()

    print('')

    print(test_graph[0])

    all_paths = dfs_graph_scan(test_graph, 'Node01')
    print(all_paths)

def question2_graph_test():
    total_graph = load_graph()

    # print("XXX Testing 4 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node04'], total_graph['Node06'])
    print("Final Result 4 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 2 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node02'], total_graph['Node06'])
    print("Final Result 2 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 3 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node03'], total_graph['Node06'])
    print("Final Result 3 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 7 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node07'], total_graph['Node06'])
    print("Final Result 7 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 8 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node08'], total_graph['Node06'])
    print("Final Result 8 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 1 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node01'], total_graph['Node06'])
    print("Final Result 1 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 5 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node05'], total_graph['Node06'])
    print("Final Result 5 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 10 - 6 ")
    weight = total_graph.find_shortest_path(total_graph['Node10'], total_graph['Node06'])
    print("Final Result 10 - 6: {} \n".format(str(weight)))

    # print("XXX Testing 6 - 10 ")
    weight = total_graph.find_shortest_path(total_graph['Node06'], total_graph['Node10'])
    print("Final Result 6 - 10: {} \n".format(str(weight)))


    test_name = "find shortest path 7 - 6"
    expected_result = 10

    actual_result = total_graph.find_shortest_path(total_graph['Node07'], total_graph['Node06'])
    test_output = "Ok" if actual_result == expected_result else "Failed"
    print("Test {} [ {} ] Expected result: [ {} ] Actual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))


    test_name = "find shortest path 6 - 1"
    expected_result = None

    actual_result = total_graph.find_shortest_path(total_graph['Node06'], total_graph['Node01'])
    test_output = "Ok" if actual_result == expected_result else "Failed"
    print("Test {} [ {} ] Expected result: [ {} ] Actual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))

    test_name = "find shortest path 8 - 4"
    expected_result = 25

    actual_result = total_graph.find_shortest_path(total_graph['Node08'], total_graph['Node04'])
    test_output = "Ok" if actual_result == expected_result else "Failed"
    print("Test {} [ {} ] Expected result: [ {} ] Actual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))

    test_name = "find shortest path 9 - 4"
    expected_result = 25

    actual_result = total_graph.find_shortest_path(total_graph['Node09'], total_graph['Node04'])
    test_output = "Ok" if actual_result == expected_result else "Failed"
    print("Test {} [ {} ] Expected result: [ {} ] Actual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))


    print("total_graph.sort_number_reachable_node:", total_graph.sort_number_reachable_node())

    # print total_graph.shortest_path_highest_weight()
    # print("The length of graph is: {}".format(len(new_graph)))

def test_class_graph():
    node02_new = Node('Node02')
    node03_new = Node('Node03')
    node03_new.add_neighbor('Node02', 11)
    node03_new.add_neighbor('Node04', 10)
    node03_new.add_neighbor('Node07', 50)
    node03_new.add_neighbor('Node09', 20)

    node07_new = Node('Node07')
    node07_new.add_neighbor('Node02', 11)
    node09_new = Node('Node09')

    new_graph = Graph('test',[node03_new])

    new_graph.add_node(node07_new)
    new_graph.add_node(node09_new)
    print(new_graph)

    # graph02 = new_graph + node07_new
    # print(graph02)

    # new_graph.remove_node('Node07')
    # print (new_graph)

    print new_graph.is_edge('Node03', 'Node02')

    # print new_graph.add_edge('Node07', 'Node02', 5)
    print new_graph.add_edge('Node03', 'Node07', 52)
    new_graph.add_edge('Node02', 'Node03', 6)
    print (new_graph)

    # print(new_graph.remove_edge('Node07', 'Node02'))
    # print(new_graph)

    print(new_graph.get_edge_weight('Node03', 'Node07'))

    print (new_graph.get_path_weight(['Node03','Node07','Node02']))

    print new_graph.sort_number_reachable_node()

def test_is_reachable(total_graph):
    print total_graph.shortest_path_highest_weight()
    node01 = total_graph['Node01']
    node03 = total_graph['Node03']
    node04 = total_graph['Node04']
    node06 = total_graph['Node06']
    node07 = total_graph['Node07']
    node08 = total_graph['Node08']
    node09 = total_graph['Node09']
    node10 = total_graph['Node10']

    # is_reachable(self, frm_name, to_name, visited=[]) test 01
    test_name = "Check is_reachable from node 1 to node 3"
    expected_result = True
    actual_result = total_graph.is_reachable(node01, node03)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))
    # is_reachable(self, frm_name, to_name, visited=[]) test 02
    test_name = "Check is_reachable from node 6 to node 9"
    expected_result = False
    actual_result = total_graph.is_reachable(node06, node09)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))
    # is_reachable(self, frm_name, to_name, visited=[]) test 03
    test_name = "Check is_reachable from node 9 to node 6"
    expected_result = True
    actual_result = total_graph.is_reachable(node09, node06)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                test_name,
                                                                                str(expected_result),
                                                                                str(actual_result)))
    # is_reachable(self, frm_name, to_name, visited=[]) test 04
    test_name = "Check is_reachable from node 9 to node 1"
    expected_result = True
    actual_result = total_graph.is_reachable(node09, node01)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                        test_name,
                                                                                        str(expected_result),
                                                                                        str(actual_result)))

    # is_reachable(self, frm_name, to_name, visited=[]) test 05
    test_name = "Check is_reachable from node 10 to node 1"
    expected_result = False
    actual_result = total_graph.is_reachable(node10, node01)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                        test_name,
                                                                                        str(expected_result),
                                                                                        str(actual_result)))

    # is_reachable(self, frm_name, to_name, visited=[]) test 06
    test_name = "Check is_reachable from node 4 to node 9"
    expected_result = False
    actual_result = total_graph.is_reachable(node04, node09)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                        test_name,
                                                                                        str(expected_result),
                                                                                        str(actual_result)))

    # is_reachable(self, frm_name, to_name, visited=[]) test 07
    test_name = "Check is_reachable from node 8 to node 4"
    expected_result = True
    actual_result = total_graph.is_reachable(node08, node04)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                        test_name,
                                                                                        str(expected_result),
                                                                                        str(actual_result)))

    # is_reachable(self, frm_name, to_name, visited=[]) test 08
    test_name = "Check is_reachable from node 10 to node 6"
    expected_result = True
    actual_result = total_graph.is_reachable(node10, node06)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                        test_name,
                                                                                        str(expected_result),
                                                                                        str(actual_result)))

    # is_reachable(self, frm_name, to_name, visited=[]) test 08
    test_name = "Check is_reachable from node 10 to node 7"
    expected_result = False
    actual_result = total_graph.is_reachable(node10, node07)
    test_output = "Success" if actual_result == expected_result else "Failed"
    print("Test {} \t\t[ {} ] \tExpected result: [ {} ] \tActual result: [ {} ]".format(test_output,
                                                                                        test_name,
                                                                                        str(expected_result),
                                                                                        str(actual_result)))

def check_get_all_paths():
    my_graph = load_graph("my_graph")
    all_paths_list = my_graph.get_all_paths(my_graph["Node08"], my_graph["Node06"])

    for path in all_paths_list:
        print "Path ", type(path), path
        for item in path:
            print(item.name)
        print(my_graph.get_path_weight(path))


# main
if __name__ == "__main__":
    test_graph = load_graph('test')
    # print(type(test_graph.nodes))
    # print(json.dumps(test_graph.graph_as_defaultdict()))
    # question_3()
    # question_4()
    # check_get_all_paths()
    check_longest_graph()

    # question2_graph_test()
    # test_is_reachable(load_graph())
    # test_class_graph()
    # test_is_reachable(load_graph())