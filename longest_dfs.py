from collections import defaultdict


def dfs_graph_scan(graph_to_scan, vertex, seen=None, path=None):
    if seen is None:
        seen = []
    if path is None:
        path = [vertex]

    seen.append(vertex)

    paths = []
    for target in graph_to_scan[vertex]:
        if target not in seen:
            target_path = path + [target]
            paths.append(tuple(target_path))
            paths.extend(dfs_graph_scan(graph_to_scan, target, seen[:], target_path))
    return paths


# Define graph by edges
edges = [['1', '2'], ['2', '4'], ['1', '11'], ['4', '11']]

# Build graph dictionary
graph_to_scan = defaultdict(list)
for (source, target) in edges:
    graph_to_scan[source].append(target)
    graph_to_scan[target].append(source)

print(graph_to_scan)
# Run DFS, compute metrics
all_paths = dfs_graph_scan(graph_to_scan, '1')
max_len   = max(len(path) for path in all_paths)
max_paths = [path for path in all_paths if len(path) == max_len]

# Output
print("All Paths:")
print(type(all_paths), all_paths)
print("Longest Paths:")
for path in max_paths:
    print("  ", path)
print("Longest Path Length:")
print(max_len)
