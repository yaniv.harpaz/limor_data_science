import datetime


class RunningTimer(object):
    # Class for basic timer management
    def __init__(self, remove_micro_seconds=False):
        self.end_timer = None
        if remove_micro_seconds:
            self.timer = datetime.datetime.now().replace(microsecond=0)
            self.remove_micro_seconds = remove_micro_seconds
        else:
            self.timer = datetime.datetime.now()
            self.remove_micro_seconds = remove_micro_seconds

    def restart(self, remove_micro_seconds=False):
        if remove_micro_seconds:
            self.timer = datetime.datetime.now().replace(microsecond=0)
        else:
            self.timer = datetime.datetime.now()

    def stop(self):
        if self.remove_micro_seconds:
            self.end_timer = datetime.datetime.now().replace(microsecond=0)
        else:
            self.end_timer = datetime.datetime.now()

    def display(self):
        if self.end_timer is None:
            return "The timer did not stop"
        result = str(self.end_timer - self.timer)
        return result

