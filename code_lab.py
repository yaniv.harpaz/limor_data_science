def find_shortest_path_all(self, frm_name):
    # Use BFS to check path between s and d
    # Mark all the vertices as not visited
    nodes_not_visited = list(self.nodes)
    distances = dict(zip(list(self.nodes), [-1] * len(self.nodes)))
    last_vertex = dict(zip(list(self.nodes), [-1] * len(self.nodes)))

    # Create a queue for BFS
    queue = []

    # Mark the source node as visited and enqueue it
    queue.append(frm_name)
    nodes_not_visited.remove(frm_name)
    distances[frm_name] = 0
    last_vertex[frm_name] = frm_name

    while queue:
        # Dequeue a vertex from queue
        n = queue.pop(0)

        # If this adjacent node is the destination node,
        # then return true
        # if n == to_name:
        #     return True

        # Else, continue to do BFS
        for i in self.nodes[n].neighbors:
            if i in nodes_not_visited:
                queue.append(i)
                nodes_not_visited.remove(i)

            if (distances[i] == -1) or (distances[i] > distances[n] + self.get_edge_weight(n, i)):
                distances[i] = distances[n] + self.get_edge_weight(n, i)
                last_vertex[i] = n

                # there is a shorter path

    return distances, last_vertex


def loop_without_loop(n):
    print("Runnin with ", n)
    result = 0
    if n == 1:
        result = 1
    else:
        result = loop_without_loop(n - 1) + 1

    print(result)
    return result



if __name__ == "__main__":
    loop_without_loop(5)