from node import Node
from graph import Graph
import csv
import datetime
# import settings
import json

class RoadmapSettings(object):
    def __init__(self):
        self.input_files = {
            'EW': r'T:\Users\yaniv\GoogleDrive\limor\DS\Part 1 - Python programming\Python programming - final project\travelsEW.csv',
            'WE': r'T:\Users\yaniv\GoogleDrive\limor\DS\Part 1 - Python programming\Python programming - final project\travelsWE.csv',
        }

        self.relevant_nodes = ['Center', 'North', 'South', 'East', 'West']

roadmap_settings = RoadmapSettings()

def build_travel_graph_edges(dict_nodes_weight):
    regions_file1 = Graph("Regions", [Node('Center'), Node('North'), Node('South'), Node('East'), Node('West')])
    for direction in dict_nodes_weight:
        amount_of_times = len(dict_nodes_weight[direction])
        sum_time = sum(dict_nodes_weight[direction])
        if amount_of_times != 0:
            regions_file1[direction[0]].add_neighbor(direction[1], sum_time / amount_of_times)
            sum_time = 0
    # print(dict_nodes_weight)
    # print(regions_file1)
    return regions_file1


def travel_file_to_dict(travel_format):
    input_file = roadmap_settings.input_files[travel_format]
    list_relevant_nodes = roadmap_settings.relevant_nodes
    dict_nodes_weight = dict()
    with open(input_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['From'] not in list_relevant_nodes or row['To'] not in list_relevant_nodes:
                continue
            else:
                try:
                    if travel_format == 'EW':
                        time_second = (datetime.datetime.strptime(row['Tstop'], '%d/%m/%Y %Hh%Mm') - \
                                       datetime.datetime.strptime(row['Tstart'], '%d/%m/%Y %Hh%Mm')).total_seconds()
                    elif travel_format == 'WE':
                        time_second = (datetime.datetime.strptime(row['Tstop'], '%H:%M:%S%p ; %b %d %y') - \
                                       datetime.datetime.strptime(row['Tstart'],
                                                                  '%H:%M:%S%p ; %b %d %y')).total_seconds()
                    else:
                        print("Error in travel format {}".format(travel_format))

                    if (row['From'], row['To']) in dict_nodes_weight:
                        dict_nodes_weight[(row['From'], row['To'])].append(time_second)

                    elif (row['From'], row['To']) not in dict_nodes_weight:
                        dict_nodes_weight[(row['From'], row['To'])] = list()
                        dict_nodes_weight[(row['From'], row['To'])].append(time_second)
                except ValueError:
                    print ("Value Error! Data :", travel_format, row)

    return dict_nodes_weight


def main():
    travel_format = 'EW'
    dict_nodes_weight = travel_file_to_dict(travel_format)
    regions_file1 = build_travel_graph_edges(dict_nodes_weight)

    travel_format = 'WE'
    dict_nodes_weight = travel_file_to_dict(travel_format)
    regions_file2 = build_travel_graph_edges(dict_nodes_weight)

    total_roadmap_graph = regions_file1 + regions_file2

    max_distance = {'max_distance_value': 0,
                    'max_distance_members_name': [],
                    }
    for source_node in total_roadmap_graph.nodes.values():
        for target_node in total_roadmap_graph.nodes.values():
            if source_node is target_node:
                continue
            distance = total_roadmap_graph.find_longest_path_bfs(source_node, target_node)
            if distance is not None and distance > max_distance['max_distance_value']:
                max_distance['max_distance_value'] = distance
                max_distance['max_distance_members_name'] = [source_node.name, target_node.name]

    print "\n", max_distance, max_distance['max_distance_members_name']

    # Question 2
    # direction_list = list()
    # for region_from, dict_neighbors in total_roadmap_graph.nodes.items():
    #     for region_to, time in dict_neighbors.neighbors.items():
    #         direction_list.append((region_from, region_to, time))
    # region_from, region_to, time = sorted(direction_list, key=lambda x: x[2], reverse=True)[0]
    # print "From {} to {} takes the longest time to travel {} seconds.".format(region_from, region_to, time)

    # print find_longest_path_all_regions(total_roadmap_graph)
    # print find_longest_path(total_roadmap_graph, 'Center')
    # print find_longest_path_all_regions(total_roadmap_graph)


if __name__ == "__main__":
    main()
