from node import Node
from graph import Graph
import csv
import sys
import datetime

file_travelsEW_full_path = r'C:\Users\yanivharpaz\Dropbox\Oracle_Laptop\limor\DS\Part 1 - Python programming\Python programming - final project\travelsEW.csv'

# LINES_LIMIT = 20
# lines_counter = 0

dict_nodes_weight = dict()
list_relevant_nodes = ['Center', 'North', 'South', 'East', 'West']
CN_list = list()
CS_list = list()
CE_list = list()
CW_list = list()

NC_list = list()
NS_list = list()
NE_list = list()
NW_list = list()

SC_list = list()
SN_list = list()
SE_list = list()
SW_list = list()

EC_list = list()
EN_list = list()
ES_list = list()
EW_list = list()

WC_list = list()
WN_list = list()
WS_list = list()
WE_list = list()

dict_nodes_weight = {
    ('Center','North'): CN_list,
    ('Center','South'): CS_list,
    ('Center','East'): CE_list,
    ('Center','West'): CW_list,
    ('North','Center'): NC_list,
    ('North', 'South'): NS_list,
    ('North', 'East'): NE_list,
    ('North', 'West'): NW_list,
    ('South', 'Center'): SC_list,
    ('South', 'North'): SN_list,
    ('South', 'East'): SE_list,
    ('South', 'West'): SW_list,
    ('East', 'Center'): EC_list,
    ('East', 'North'): EN_list,
    ('East', 'South'): ES_list,
    ('East', 'West'): EW_list,
    ('West', 'Center'): WC_list,
    ('West', 'North'): WN_list,
    ('West', 'South'): WS_list,
    ('West', 'East'): WE_list
}

with open(file_travelsEW_full_path) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row['From'] not in list_relevant_nodes or row['To'] not in list_relevant_nodes:
            continue
        else:
            try:
                time_second = datetime.datetime.strptime(row['Tstop'], '%d/%m/%Y %Hh%Mm') - \
                              datetime.datetime.strptime(row['Tstart'], '%d/%m/%Y %Hh%Mm')
                dict_nodes_weight[(row['From'], row['To'])].append(time_second.seconds)

            except ValueError:
                pass

    node_center = Node('Center')
    node_north = Node('North')
    node_south = Node('South')
    node_east = Node('East')
    node_west = Node('West')

    roadmap_graph_file1 = Graph('roadmap', [node_center, node_north, node_south, node_east, node_west])

    sum_time = 0
    for direction in dict_nodes_weight:
        mount_of_road = len(dict_nodes_weight[direction])
        for time_road in dict_nodes_weight[direction]:
            sum_time += time_road
        if mount_of_road != 0:
            roadmap_graph_file1[direction[0]].add_neighbor(direction[1], sum_time/mount_of_road)

    print(dict_nodes_weight)

    print(roadmap_graph_file1)


file_travelsWE_full_path = r'C:\Users\yanivharpaz\Dropbox\Oracle_Laptop\limor\DS\Part 1 - Python programming\Python programming - final project\travelsWE.csv'


dict_nodes_weight = dict()
list_relevant_nodes = ['Center', 'North', 'South', 'East', 'West']
CN_list = list()
CS_list = list()
CE_list = list()
CW_list = list()

NC_list = list()
NS_list = list()
NE_list = list()
NW_list = list()

SC_list = list()
SN_list = list()
SE_list = list()
SW_list = list()

EC_list = list()
EN_list = list()
ES_list = list()
EW_list = list()

WC_list = list()
WN_list = list()
WS_list = list()
WE_list = list()

dict_nodes_weight = {
    ('Center','North'): CN_list,
    ('Center','South'): CS_list,
    ('Center','East'): CE_list,
    ('Center','West'): CW_list,
    ('North','Center'): NC_list,
    ('North', 'South'): NS_list,
    ('North', 'East'): NE_list,
    ('North', 'West'): NW_list,
    ('South', 'Center'): SC_list,
    ('South', 'North'): SN_list,
    ('South', 'East'): SE_list,
    ('South', 'West'): SW_list,
    ('East', 'Center'): EC_list,
    ('East', 'North'): EN_list,
    ('East', 'South'): ES_list,
    ('East', 'West'): EW_list,
    ('West', 'Center'): WC_list,
    ('West', 'North'): WN_list,
    ('West', 'South'): WS_list,
    ('West', 'East'): WE_list
}

with open(file_travelsWE_full_path) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row['From'] not in list_relevant_nodes or row['To'] not in list_relevant_nodes:
            continue
        else:
            try:
                time_second = datetime.datetime.strptime(row['Tstop'], '%H:%M:%S%p ; %b %d %y') - \
                              datetime.datetime.strptime(row['Tstart'], '%H:%M:%S%p ; %b %d %y')
                dict_nodes_weight[(row['From'], row['To'])].append(time_second.seconds)

            except ValueError:
                pass

    node_center = Node('Center')
    node_north = Node('North')
    node_south = Node('South')
    node_east = Node('East')
    node_west = Node('West')

    roadmap_graph_file2 = Graph('roadmap', [node_center, node_north, node_south, node_east, node_west])

    sum_time = 0
    for direction in dict_nodes_weight:
        mount_of_road = len(dict_nodes_weight[direction])
        for time_road in dict_nodes_weight[direction]:
            sum_time += time_road
        if mount_of_road != 0:
            roadmap_graph_file2[direction[0]].add_neighbor(direction[1], sum_time/mount_of_road)

    print(dict_nodes_weight)

    print(roadmap_graph_file2)

    total_roadmap_graph = roadmap_graph_file1 + roadmap_graph_file2

    print(total_roadmap_graph)




#         lines_counter += 1
#         if lines_counter > LINES_LIMIT:
#             print("Reached my limit.")
#             break
#         print("{} \t|\t {} \t|\t {} \t|\t {} ".format(row['From'], row['Tstart'], row['To'], row['Tstop']))
#
# sys.exit()

# >>> import csv
# >>> with open('names.csv') as csvfile:
# ...     reader = csv.DictReader(csvfile)
# ...     for row in reader:
# ...         print(row['first_name'], row['last_name'])
# ...


# file_full_path = r'C:\Users\yanivharpaz\Dropbox\Oracle_Laptop\limor\DS\Part 1 - Python programming\Python programming - final project\travelsEW.csv'
# list_relevant_nodes = ['Center', 'North', 'South', 'East', 'West']
# dict_nodes = dict()
# with open(file_full_path, 'r') as f:
#     for line in f:
#         try:
#             list_line = line.split(',')
#             if list_line[0] in list_relevant_nodes and\
#                list_line[2] in list_relevant_nodes:
#                 if list_line[0] not in dict_nodes:
#                     dict_nodes[list_line[0]] = Node(list_line[0])
#                     dict_nodes[list_line[0]].neighbors[list_line[2]] = ''
#                 if list_line[2] not in dict_nodes:
#                     dict_nodes[list_line[2]] = Node(list_line[2])
#
#             if list_line[0] in list_relevant_nodes and\
#                list_line[2] in list_relevant_nodes and\
#                list_line[0] in dict_nodes:
#                 dict_nodes[list_line[0]].neighbors[list_line[2]] = ''
#
#         except KeyError as err:
#             pass
#     print(dict_nodes['Center'])



